﻿namespace AutoControl3DGames.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ia : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EvenOlds", "DateCreate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EvenOlds", "DateCreate");
        }
    }
}
