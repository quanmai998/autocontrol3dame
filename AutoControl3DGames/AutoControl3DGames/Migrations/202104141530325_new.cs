﻿namespace AutoControl3DGames.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EvenOlds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Name = c.String(),
                        NameFull = c.String(),
                        NumberGuess = c.Int(nullable: false),
                        NameGuess = c.String(),
                        ResultAnswer = c.String(),
                        CountResultAnswer = c.Int(nullable: false),
                        ListNames = c.String(),
                        ListNameFulls = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TestDbs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TestDbs");
            DropTable("dbo.EvenOlds");
        }
    }
}
