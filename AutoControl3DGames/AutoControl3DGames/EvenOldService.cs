﻿using AutoControl3DGames.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace AutoControl3DGames
{
    public class EvenOldService
    {
        public static readonly ModelContext _contextDb = new ModelContext();

        public static readonly EvenOldRepository evenOldRepository = new EvenOldRepository(_contextDb);

        public static List<string> listEvenOldNames = new List<string>();
        public static List<string> listEvenOldNameFulls = new List<string>();

        public EvenOld GetAllEvenOldByListName(string listNames, string listNameFulls)
        {
            var listEvenOld = evenOldRepository.GetAllEvenOldByListName(listNames, listNameFulls).ToList();

            EvenOld evenOld = new EvenOld();

            if (listEvenOld.Count == 1)
            {
                evenOld = listEvenOld.FirstOrDefault();
            }
            else if (listEvenOld.Count > 1)
            {
                var listChan = listEvenOld.Where(x => x.Name == Constans.CHAN).ToList();
                var listLe = listEvenOld.Where(x => x.Name == Constans.LE).ToList();

                if (listChan.Count > listLe.Count)
                {
                    evenOld = listChan.Where(x => x.Name == Constans.CHAN)
                        .OrderByDescending(a => a.CountResultAnswer).FirstOrDefault();
                }
                else if (listChan.Count < listLe.Count)
                {
                    evenOld = listLe.Where(x => x.Name == Constans.LE)
                        .OrderByDescending(a => a.CountResultAnswer).FirstOrDefault();
                }
            }

            if (evenOld.Name == null || evenOld.Name == string.Empty)
            {
                return null;
            }
            return evenOld;
        }

        /// <summary>
        /// Add Upadte Even Old
        /// </summary>
        /// <param name="evenOldRelTime"></param>
        /// <param name="evenOldAnserSql"></param>
        public void AddUpadteEvenOld(EvenOld evenOldRelTime, EvenOld evenOldAnserSql)
        {
            if (listEvenOldNames.Count < Constans.countList && listEvenOldNameFulls.Count < Constans.countList)
            {            
                if (!string.IsNullOrEmpty(evenOldRelTime.Name))
                {
                    listEvenOldNames.Add(evenOldRelTime.Name);
                    listEvenOldNameFulls.Add(evenOldRelTime.NameFull);
                }
            }
            else if (listEvenOldNames.Count == Constans.countList && listEvenOldNameFulls.Count == Constans.countList)
            {
                Constans.countIndex++;
                string listNames = string.Join(Constans.SPACE, listEvenOldNames);
                string listNameFulls = string.Join(Constans.SPACE, listEvenOldNameFulls);

                evenOldRelTime.ListNames = listNames;
                evenOldRelTime.ListNameFulls = listNameFulls;
               
                if (evenOldAnserSql != null )
                {
                    if(evenOldRelTime.Name == evenOldAnserSql.Name)
                    {
                        evenOldAnserSql.ResultAnswer = Constans.TRUE;
                        evenOldAnserSql.CountResultAnswer++;

                        evenOldRelTime.ResultAnswer = Constans.TRUE;
                        evenOldRelTime.CountResultAnswer++;
                        Constans.countRatioTrue++;
                    }

                    evenOldRelTime.NameGuess = evenOldAnserSql.Name;
                    evenOldRelTime.NumberGuess = evenOldAnserSql.Number;
                    // update
                    evenOldRepository.UpdateEvenOld(evenOldAnserSql);
                }

                // Add
                evenOldRepository.Add(evenOldRelTime);
                
                if(Constans.countIndex > Constans.MIN_INDEX + 1)
                {
                    // Save
                    evenOldRepository.SaveChanges();
                }
               
            }
        }

        /// <summary>
        /// Get Last EvenOld
        /// </summary>
        /// <returns></returns>
        public EvenOld GetLastEvenOld()
        {
           return _contextDb.EvenOlds.OrderByDescending(x=> x.Id).FirstOrDefault();
        }

        /// <summary>
        /// Remove ListName
        /// </summary>
        public void RemoveAddListName(EvenOld evenOldRelTime)
        {
            if (!string.IsNullOrEmpty(evenOldRelTime.Name))
            {
                listEvenOldNames.RemoveAt(0);
                listEvenOldNameFulls.RemoveAt(0);

                listEvenOldNames.Add(evenOldRelTime.Name);
                listEvenOldNameFulls.Add(evenOldRelTime.NameFull);
            }
        }


    }
}
