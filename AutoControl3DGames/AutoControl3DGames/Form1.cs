﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace AutoControl3DGames
{
    public partial class Form1 : Form
    {
        private Timer[] timerList = new Timer[100];
        private int timerMaxIndex = -1;
        public Form1()
        {
            InitializeComponent();
            //int aInterval = SetInterval(500, a);
            int bInterval = SetInterval(1000, b);
            //int cInterval = SetInterval(1500, c);

            SetTimeout(3500, () =>
            {
                //ClearInterval(aInterval);
            });

            SetTimeout(7000, () =>
            {
                //ClearInterval(bInterval);
            });

            SetTimeout(10500, () =>
            {
                //ClearInterval(cInterval);
            });

            // there are always multiple chrome processes, so we have to loop through all of them to find the
            // process with a Window Handle and an automation element of name "Address and search bar"
            Process[] procsChrome = Process.GetProcessesByName("chrome");
            foreach (Process chrome in procsChrome)
            {
                // the chrome process must have a window
                if (chrome.MainWindowHandle == IntPtr.Zero)
                {
                    continue;
                }

                // find the automation element
                //AutomationElement elm = AutomationElement.FromHandle(chrome.MainWindowHandle);
                //AutomationElement elmUrlBar = elm.FindFirst(TreeScope.Descendants,
                //  new PropertyCondition(AutomationElement.NameProperty, "Address and search bar"));

                //// if it can be found, get the value from the URL bar
                //if (elmUrlBar != null)
                //{
                //    AutomationPattern[] patterns = elmUrlBar.GetSupportedPatterns();
                //    if (patterns.Length > 0)
                //    {
                //        ValuePattern val = (ValuePattern)elmUrlBar.GetCurrentPattern(patterns[0]);
                //        Console.WriteLine("Chrome URL found: " + val.Current.Value);
                //    }
                //}
            }
        }

        public void a()
        {
            label1.Text += "aa";
        }

        public void b()
        {
            label1.Text += "bbb";
        }

        public void c()
        {
            label1.Text += "cccc";
        }

        public int SetInterval(int time, Action function)
        {
            Timer tmr = new Timer();
            tmr.Interval = time;
            tmr.Tick += new EventHandler(delegate (object s, EventArgs ev)
            {
                function();
            });
            tmr.Start();

            timerMaxIndex++;
            var index = timerMaxIndex;
            timerList[timerMaxIndex] = tmr;
            return index;
        }

        public int SetTimeout(int time, Action function)
        {
            Timer tmr = new Timer();
            tmr.Interval = time;
            tmr.Tick += new EventHandler(delegate (object s, EventArgs ev)
            {
                function();
                tmr.Stop();
            });
            tmr.Start();

            timerMaxIndex++;
            var index = timerMaxIndex;
            timerList[timerMaxIndex] = tmr;
            return index;
        }

        public void ClearInterval(int interval)
        {
            if (timerList[interval] == null)
                return;

            timerList[interval].Stop();
            timerList[interval] = null;
            if (interval == timerMaxIndex)
                timerMaxIndex--;
        }
    }
}
