﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AutoControl3DGames
{
    public class EvenOld
    {
        [Key]
        public int Id { get; set; }

        public int Number { get; set; }

        public string Name { get; set; }

        public string NameFull { get; set; }

        public int NumberGuess { get; set; }

        public string NameGuess { get; set; }

        public string ResultAnswer { get; set; }

        public int CountResultAnswer { get; set; } = 0;

        public DateTime DateCreate { get; set; } = DateTime.Now;


        public string ListNames { get; set; }

        public string ListNameFulls { get; set; }

    }
}
