﻿using System.Linq;

namespace AutoControl3DGames.Repositories
{
    public class CategoryRepository : Repository<TestDb>
    {
        public readonly ModelContext _contextDb = new ModelContext();
        public CategoryRepository(ModelContext context) : base(context)
        {
            _contextDb = context;
        }

        public bool CategoryExists(int categoryId)
        {
            return _contextDb.TestDbs.Any(c => c.Id == categoryId);
        }

        //public async Task<IEnumerable<TestDb>> GetAllBooksForCategory(int categoryId)
        //{
        //    return await _contextDb.TestDbs.Where(b => b.CategoryId == categoryId)
        //         .Select(c => c.Book).ToListAsync();
        //}

        //public async Task<IEnumerable<Category>> GetCategoriesForABook(int bookId)
        //{
        //    return await _contextDb.BookCategories.Where(b => b.BookId == bookId)
        //        .Select(c => c.Category).ToListAsync();
        //}

        //public async Task<bool> IsDuplicateCategoryName(int categoryId, string categoryName)
        //{
        //    var category = await _contextDb.Categories.Where(c => c.Name.Trim().ToUpper() == categoryName.Trim().ToUpper()
        //                                        && c.Id != categoryId).FirstOrDefaultAsync();

        //    return category == null ? false : true;
        //}

    }
}
