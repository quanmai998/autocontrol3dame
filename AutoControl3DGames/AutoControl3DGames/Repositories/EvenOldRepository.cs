﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace AutoControl3DGames.Repositories
{
    public class EvenOldRepository : Repository<EvenOld>
    {
        public readonly ModelContext _contextDb;
        public EvenOldRepository(ModelContext context) : base(context)
        {
            _contextDb = context;
        }

        public bool CategoryExists(int categoryId)
        {
            return _contextDb.TestDbs.Any(c => c.Id == categoryId);
        }

        public IEnumerable<EvenOld> GetAllEvenOldByListName(string listName, string listNameFull)
        {
            if (listName == null)
            {
                return _contextDb.EvenOlds.Where(b => b.ListNames == listNameFull).ToList();
            }
            else
            {
                return _contextDb.EvenOlds.Where(b => b.ListNames == listName).ToList();
            }
        }

        public void UpdateEvenOld(EvenOld evenOldAnserSql)
        {
            _contextDb.Entry(evenOldAnserSql).State = EntityState.Modified;
        }
    }
}
