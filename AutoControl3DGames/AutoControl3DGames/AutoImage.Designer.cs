﻿namespace AutoControl3DGames
{
    partial class AutoImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelKQRealTime = new System.Windows.Forms.Label();
            this.labelKqDB = new System.Windows.Forms.Label();
            this.labelKqRTime = new System.Windows.Forms.Label();
            this.labelKQDBase = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelRadio = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelKQRealTime
            // 
            this.labelKQRealTime.AutoSize = true;
            this.labelKQRealTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKQRealTime.Location = new System.Drawing.Point(12, 9);
            this.labelKQRealTime.Name = "labelKQRealTime";
            this.labelKQRealTime.Size = new System.Drawing.Size(135, 25);
            this.labelKQRealTime.TabIndex = 0;
            this.labelKQRealTime.Text = "KQ Real Time";
            // 
            // labelKqDB
            // 
            this.labelKqDB.AutoSize = true;
            this.labelKqDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKqDB.Location = new System.Drawing.Point(12, 69);
            this.labelKqDB.Name = "labelKqDB";
            this.labelKqDB.Size = new System.Drawing.Size(74, 25);
            this.labelKqDB.TabIndex = 2;
            this.labelKqDB.Text = "KQ DB";
            // 
            // labelKqRTime
            // 
            this.labelKqRTime.AutoSize = true;
            this.labelKqRTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKqRTime.Location = new System.Drawing.Point(173, 9);
            this.labelKqRTime.Name = "labelKqRTime";
            this.labelKqRTime.Size = new System.Drawing.Size(0, 25);
            this.labelKqRTime.TabIndex = 4;
            // 
            // labelKQDBase
            // 
            this.labelKQDBase.AutoSize = true;
            this.labelKQDBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKQDBase.Location = new System.Drawing.Point(173, 69);
            this.labelKQDBase.Name = "labelKQDBase";
            this.labelKQDBase.Size = new System.Drawing.Size(0, 25);
            this.labelKQDBase.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "% KQ True";
            // 
            // labelRadio
            // 
            this.labelRadio.AutoSize = true;
            this.labelRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRadio.Location = new System.Drawing.Point(173, 131);
            this.labelRadio.Name = "labelRadio";
            this.labelRadio.Size = new System.Drawing.Size(0, 25);
            this.labelRadio.TabIndex = 7;
            // 
            // AutoImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 192);
            this.Controls.Add(this.labelRadio);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelKQDBase);
            this.Controls.Add(this.labelKqRTime);
            this.Controls.Add(this.labelKqDB);
            this.Controls.Add(this.labelKQRealTime);
            this.Location = new System.Drawing.Point(0, 350);
            this.Name = "AutoImage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "AutoImage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelKQRealTime;
        private System.Windows.Forms.Label labelKqDB;
        private System.Windows.Forms.Label labelKqRTime;
        private System.Windows.Forms.Label labelKQDBase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelRadio;
    }
}