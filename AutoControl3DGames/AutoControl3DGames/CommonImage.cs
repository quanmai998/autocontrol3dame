﻿using KAutoHelper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace AutoControl3DGames
{
    public class CommonImage
    {
        private EvenOldService evenOldService = new EvenOldService();

        private static int startRealTimeX = 485;
        private static int startRealTimeY = 701;
        private static EvenOld evenOldAnserSql = null;

        /// <summary>
        /// Get Last EvenOld
        /// </summary>
        /// <returns></returns>
        public EvenOld GetLastEvenOld()
        {
            return evenOldService.GetLastEvenOld();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="evenOldRealTime"></param>
        /// <returns></returns>
        public EvenOld CallDbCalutorImg(EvenOld evenOldRealTime)
        {
            if (evenOldRealTime.Name != null && evenOldRealTime.Name != "")
            {
                evenOldService.AddUpadteEvenOld(evenOldRealTime, evenOldAnserSql);
            }

            if (EvenOldService.listEvenOldNames.Count == Constans.countList
                && EvenOldService.listEvenOldNameFulls.Count == Constans.countList)
            {
                evenOldService.RemoveAddListName(evenOldRealTime);
                string listNames = string.Join(Constans.SPACE, EvenOldService.listEvenOldNames);
                string listNameFulls = string.Join(Constans.SPACE, EvenOldService.listEvenOldNameFulls);

                evenOldAnserSql = evenOldService.GetAllEvenOldByListName(listNames, listNameFulls);
                //evenOldAnserSql = evenOldService.GetAllEvenOldByListName(null, listNameFulls);
            }
            return evenOldAnserSql;
        }


        /// <summary>
        /// Get Img RealTime
        /// </summary>
        public EvenOld GetImgRealTime()
        {
            var main = (Bitmap)CaptureHelper.CaptureScreen();

            int pixelItem = 24;
            int lengthRow = 6;
            int pixelSml = 2;
            startRealTimeX = 484;
            startRealTimeY = 700;

            //var mainRealTime = CaptureHelper.CropImage(main, new Rectangle(484, 700 + pixelItem, 24, 24));
            bool checkImgNew = false;
            EvenOld evenOld = null;
            for (int count = 1; count < 7; count++)
            {
                int pixel = pixelItem * count;

                var mainRealTime = CaptureHelper.CropImage(main,
                    new Rectangle(startRealTimeX + pixelSml,
                    startRealTimeY + pixel + pixelSml,
                    pixelItem - pixelSml,
                    pixelItem - pixelSml));
                //mainRealTime.Save($"anhviRtime{count}.png");
                bool checkEmpty = CheckIsEmpty(mainRealTime);

                if (!checkEmpty)
                {
                    evenOld = CompareImageEvenOld(mainRealTime);

                    if (evenOld.Name == null)
                    {
                        if (startRealTimeX == 474)
                        {
                            startRealTimeX += 10;
                        }
                        else if (startRealTimeX == 484)
                        {
                            startRealTimeX -= 10;
                        }
                        mainRealTime = CaptureHelper.CropImage(main,
                          new Rectangle(startRealTimeX + pixelSml,
                          startRealTimeY + pixel + pixelSml,
                          pixelItem - pixelSml,
                          pixelItem - pixelSml));

                        evenOld = CompareImageEvenOld(mainRealTime);
                    }

                    if (count == lengthRow)
                    {
                        checkImgNew = true;
                    }
                }
                else
                {
                    checkImgNew = true;
                }

                if (checkImgNew)
                {
                    return evenOld;
                }

            }
            return null;
        }


        /// <summary>
        /// Check Start Time
        /// </summary>
        /// <returns></returns>
        public bool CheckStartTime(int timerIndex)
        {
            if (timerIndex == Constans.MIN_INDEX)
            {
                var main = (Bitmap)CaptureHelper.CaptureScreen();
                var imageCompare = (Bitmap)Bitmap.FromFile("time16ml.png");
                var point = ImageScanOpenCV.FindOutPoint(main, imageCompare);

                if (point != null)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Get Point Time End Popup
        /// </summary>
        /// <returns></returns>
        public int[] CommonGetPoint3DGames(string nameImage)
        {
            int[] arrInt = null;

            var imageCompare = (Bitmap)Bitmap.FromFile(nameImage);
            arrInt = GetPointCommon(imageCompare, Constans.MIN_INDEX);

            return arrInt;
        }

        /// <summary>
        /// Get Point Time End Popup
        /// </summary>
        /// <returns></returns>
        public int[] GetPointTimeEndPopup(int timerIndex)
        {
            int[] arrInt = null;
            if (timerIndex == Constans.MIN_INDEX)
            {
                var imageCompare = (Bitmap)Bitmap.FromFile("xacnhan.png");
                arrInt = GetPointCommon(imageCompare, timerIndex);
            }
            return arrInt;
        }

        /// <summary>
        /// Get Point 3D Game
        /// </summary>
        /// <returns></returns>
        public int[] GetPoint3DGame(int timerIndex)
        {
            int[] arrInt = null;
            if (timerIndex == Constans.MIN_INDEX)
            {
                var imageCompare = (Bitmap)Bitmap.FromFile("3DGameXD.png");
                arrInt = GetPointCommon(imageCompare, timerIndex);
            }
            return arrInt;
        }

        /// <summary>
        /// Get Point 3D Game
        /// </summary>
        /// <returns></returns>
        public int[] GetPoint3DGameTable(int timerIndex)
        {
            int[] arrInt = null;
            if (timerIndex == Constans.MIN_INDEX)
            {
                var imageCompare = (Bitmap)Bitmap.FromFile("table1.png");
                arrInt = GetPointCommon(imageCompare, timerIndex);
            }
            return arrInt;
        }

        /// <summary>
        /// Get Point Common
        /// </summary>
        /// <param name="imageCompare"></param>
        /// <param name="timerIndex"></param>
        /// <returns></returns>
        private int[] GetPointCommon(Bitmap imageCompare, int timerIndex)
        {
            int[] arrInt = null;
            if (timerIndex == Constans.MIN_INDEX)
            {
                Size size = new Size();
                size.Width = 1920;
                size.Height = 1080;
                Point pointImg = new Point();
                pointImg.X = 0;
                pointImg.Y = 0;

                var main = (Bitmap)CaptureHelper.CaptureImage(size, pointImg);
                main.Save("imgCommon.png");
                imageCompare.Save("imgCommonCom.png");
                var point = ImageScanOpenCV.FindOutPoint(main, imageCompare);

                if (point != null)
                {
                    arrInt = new int[2];
                    arrInt[0] = point.Value.X;
                    arrInt[1] = point.Value.Y;
                    return arrInt;
                }
            }
            return arrInt;
        }

        /// <summary>
        /// CheckI s Empty
        /// </summary>
        /// <param name="imageRealTime"></param>
        /// <returns></returns>
        public bool CheckIsEmpty(Bitmap imageRealTime)
        {
            var imageCompare = (Bitmap)Bitmap.FromFile("vi5.png");
            var point = ImageScanOpenCV.FindOutPoint(imageCompare, imageRealTime);

            if (point != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageRealTime"></param>
        /// <returns></returns>
        public EvenOld CompareImageEvenOld(Bitmap imageRealTime)
        {
            EvenOld evenOld = new EvenOld();

            List<string> listFile = new List<string>();

            for (int i = 0; i < 5; i++)
            {
                listFile.Add($"vi{i}.png");
            }

            foreach (string fileName in listFile)
            {
                var imageCompare = (Bitmap)Bitmap.FromFile(fileName);
                var point = ImageScanOpenCV.FindOutPoint(imageCompare, imageRealTime);

                if (point != null)
                {
                    string nameImage = string.Empty;

                    string valueRealTime = Regex.Replace(fileName, @"[^0-9]", string.Empty);

                    int number;

                    bool success = Int32.TryParse(valueRealTime, out number);
                    if (success)
                    {

                        if (number % 2 == 0)
                        {
                            nameImage = Constans.CHAN;

                        }
                        else
                        {
                            nameImage = Constans.LE;
                        }
                        evenOld.Name = nameImage;


                        if (number == 2)
                        {
                            nameImage += $" {Constans.S2} ";
                        }
                        else if (number > 2)
                        {
                            nameImage += $" {Constans.TAI} ";
                        }
                        else if (number < 2)
                        {
                            nameImage += $" {Constans.XIU} ";
                        }
                        evenOld.NameFull = nameImage;

                        evenOld.Number = number;
                    }
                    break;
                }
            }
            return evenOld;
        }
    }
}
