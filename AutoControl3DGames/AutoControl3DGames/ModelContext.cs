﻿using System.Data.Entity;

namespace AutoControl3DGames
{
    public class ModelContext : DbContext
    {
        public ModelContext() : base("name = con")
        {

        }

        public virtual DbSet<TestDb> TestDbs { get; set; }

        public virtual DbSet<EvenOld> EvenOlds { get; set; }
    }
}
