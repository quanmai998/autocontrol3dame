﻿using KAutoHelper;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoControl3DGames
{
    public partial class AutoImage : Form
    {
        private CommonImage commonImage = new CommonImage();
        private static int interval;
        private static int timerIndex = Constans.MIN_INDEX;
        private static int timeInterval = 550;
        private static int timeMilisecon = 26;
        private static EvenOld evenOldRealTimeOld = null;
        bool checkOutMoney = false;

        public AutoImage()
        {
            InitializeComponent();
            LoadProceing();          
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadProceing()
        {
            try
            {
                if (timerIndex > Constans.MIN_INDEX)
                {
                    timeInterval = timeMilisecon * 1000;
                    timerIndex = Constans.MIN_INDEX;
                    Thread.Sleep((int)TimeSpan.FromSeconds(timeMilisecon).TotalMilliseconds);
                }
                interval = CommonSetTimeOut.SetInterval(timeInterval, GetStartTime, false);
            }
            catch (Exception)
            {
                Application.Restart();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public void GetStartTime()
        {
            try
            {
                int[] arrInt = commonImage.GetPointTimeEndPopup(timerIndex);

                if (arrInt != null)
                {
                    CommonSetTimeOut.timer.Dispose();

                    bool check = LoadTimeOutStart();

                    while (check)
                    {
                        check = LoadTimeOutStart();
                    }

                    timerIndex = Constans.MIN_INDEX;
                    timeInterval = 550;
                }
                else
                {
                    bool check = commonImage.CheckStartTime(timerIndex);
                    if (check)
                    {
                        timerIndex++;
                        EvenOld evenOldRealTime = commonImage.GetImgRealTime();

                        if (evenOldRealTime != null)
                        {
                            if (evenOldRealTime.Name == null && evenOldRealTimeOld != null)
                            {
                                evenOldRealTime = evenOldRealTimeOld;
                            }

                            labelKqRTime.Text = evenOldRealTime.Name + "__" + evenOldRealTime.NameFull;
                            labelKqRTime.Refresh();

                            EvenOld evenOldAnserSql = commonImage.CallDbCalutorImg(evenOldRealTime);
                            if (evenOldAnserSql != null)
                            {
                                labelKQDBase.Text = evenOldAnserSql.Name + "__" + evenOldAnserSql.NameFull + $"{evenOldAnserSql.Id}";

                                // dat cuoc neu con tien
                                if (!checkOutMoney)
                                {
                                    PlacePoint(evenOldAnserSql);
                                    Thread.Sleep((int)TimeSpan.FromSeconds(2).TotalMilliseconds);
                                    LoadCheckOutMoney();
                                }
                            }
                            else
                            {
                                labelKQDBase.Text = string.Empty;
                            }
                            labelKQDBase.Refresh();

                            if (Constans.countIndex > 0)
                            {
                                int radio = Constans.countRatioTrue / Constans.countIndex * 100;
                                labelRadio.Text = radio.ToString() + "%";
                                labelRadio.Refresh();
                            }

                            evenOldRealTimeOld = evenOldRealTime;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                LoadProceing();
            }
        }

        /// <summary>
        /// Place Point 
        /// </summary>
        /// <param name="evenOldAnserSql"></param>
        /// <param name="checkOutMoney"></param>
        private void PlacePoint(EvenOld evenOldAnserSql)
        {
            string nameImage = string.Empty;
            string nameImage16m = string.Empty;
            int x = 0;
            int y = 0;

            int[] arrIntPoint = commonImage.CommonGetPoint3DGames(Constans.POINT_CHECK);
            if(arrIntPoint == null)
            {
                arrIntPoint = commonImage.CommonGetPoint3DGames(Constans.POINT);
            }

            if (arrIntPoint != null)
            {
                x = arrIntPoint[0] - 230;
                y = arrIntPoint[1] - 135;
                // click  xu
                AutoClickMouse(x, y);
            }           

            if (evenOldAnserSql.Name == Constans.CHAN)
            {
                nameImage = "matchan.png";
                nameImage16m = "matchan16m.png";
            }
            else
            {
                nameImage = "matle.png";
                nameImage16m = "matle16m.png";
            }
            // get mat chan/le
            int[] arrInt = commonImage.CommonGetPoint3DGames(nameImage16m);

            if(arrInt == null)
            {
                arrInt = commonImage.CommonGetPoint3DGames(nameImage);
            }

            if (arrInt != null)
            {             
                x = arrInt[0] - 100;
                y = arrInt[1] - 50;
                // click mat chan/le
                AutoClickMouse(x, y);
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadCheckOutMoney()
        {
            bool check = LoadOutMoney(true);
            if (check)
            {
                checkOutMoney = true;
                while (check)
                {
                    check = LoadOutMoney(false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool LoadOutMoney(bool isCheckOutMoney)
        {
            int x = 0;
            int y = 0;
            int[] arrInt = null;

            if (isCheckOutMoney)
            {
                // check het tien
                arrInt = commonImage.CommonGetPoint3DGames("hettien.png");

                if (arrInt != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            arrInt = commonImage.CommonGetPoint3DGames("huyhettien.png");
            if (arrInt != null)
            {
                x = arrInt[0] - 100;
                y = arrInt[1] - 100;
                AutoClickMouse(x, y);

                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool LoadTimeOutStart()
        {
            int x = 0;
            int y = 0;
            int[] arrInt = commonImage.GetPointTimeEndPopup(timerIndex);

            if (arrInt != null)
            {
                x = arrInt[0] - 150;
                y = arrInt[1] - 100;
                AutoClickMouse(x, y);
            }

            arrInt = commonImage.GetPoint3DGame(timerIndex);
            if (arrInt != null)
            {
                x = arrInt[0] - 150;
                y = arrInt[1] - 100;
                AutoClickMouse(x, y);
            }

            arrInt = commonImage.GetPoint3DGameTable(timerIndex);
            if (arrInt != null)
            {
                x = arrInt[0];
                y = arrInt[1];
                AutoClickMouse(x, y);

                return false;
            }

            return true;
        }

        /// <summary>
        /// Auto Click Mouse 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void AutoClickMouse(int x, int y)
        {
            AutoControl.MouseClick(x, y, EMouseKey.LEFT);
        }
    }
}
