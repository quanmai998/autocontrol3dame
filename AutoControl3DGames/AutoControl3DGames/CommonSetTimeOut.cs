﻿using System;
using System.Windows.Forms;

namespace AutoControl3DGames
{
    public class CommonSetTimeOut
    {
        private static Timer[] timerList = new Timer[10];

        public static Timer timer = new Timer();
        private static int timerMaxIndex = Constans.MIN_INDEX;
        private static int timerIndex = 1;

        public static int SetInterval(int time, Action function, bool checkArrTimer)
        {
            Timer tmr = new Timer();
            tmr.Interval = time;
            tmr.Tick += new EventHandler(delegate (object s, EventArgs ev)
            {
                function();
            });
            tmr.Start();

            int index;

            if (checkArrTimer)
            {
                timerMaxIndex++;
                index = timerMaxIndex;
                timerList[timerMaxIndex] = tmr;
            }
            else
            {
                timer = tmr;
                index = timerIndex;             
            }

            return index;
        }

        public static int SetTimeout(int time, Action function)
        {
            Timer tmr = new Timer();
            tmr.Interval = time;
            tmr.Tick += new EventHandler(delegate (object s, EventArgs ev)
            {
                function();
                tmr.Stop();
            });
            tmr.Start();

            timerMaxIndex++;
            var index = timerMaxIndex;
            timerList[timerMaxIndex] = tmr;
            return index;
        }

        public static void ClearInterval(int interval, bool checkArrTimer)
        {
            if (checkArrTimer)
            {
                if (timerList[interval] == null)
                    return;

                timerList[interval].Stop();
                timerList[interval] = null;
                if (interval == timerMaxIndex)
                    timerMaxIndex--;
            }
            else
            {
                if (timer == null)
                    return;

                timer.Stop();
            }

        }

    }
}
