﻿using System.ComponentModel.DataAnnotations;

namespace AutoControl3DGames
{
    public class TestDb
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
